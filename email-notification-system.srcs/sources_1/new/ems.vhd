----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/23/2018 02:23:35 PM
-- Design Name: 
-- Module Name: ems - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ems is
  Port (clk     :   in std_logic;
        sw      :   in std_logic_vector(15 downto 0);
        JB      :   inout std_logic_vector(0 to 7);
        JC      :   inout std_logic_vector(0 to 7);
        JA      :   in std_logic_vector(0 to 7);
        btn     :   in std_logic_vector(5 downto 0);
        leds    :   inout std_logic_vector(15 downto 0)
        );
end ems;

architecture Behavioral of ems is
signal rdy : std_logic_vector(0 to 0);
signal count : std_logic_vector(7 downto 0);


component oledrgbpmod_wrapper is
  port (
    commandinput_tri_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    jb_pin10_io : inout STD_LOGIC;
    jb_pin1_io : inout STD_LOGIC;
    jb_pin2_io : inout STD_LOGIC;
    jb_pin3_io : inout STD_LOGIC;
    jb_pin4_io : inout STD_LOGIC;
    jb_pin7_io : inout STD_LOGIC;
    jb_pin8_io : inout STD_LOGIC;
    jb_pin9_io : inout STD_LOGIC;
    jc_pin10_io : inout STD_LOGIC;
    jc_pin1_io : inout STD_LOGIC;
    jc_pin2_io : inout STD_LOGIC;
    jc_pin3_io : inout STD_LOGIC;
    jc_pin4_io : inout STD_LOGIC;
    jc_pin7_io : inout STD_LOGIC;
    jc_pin8_io : inout STD_LOGIC;
    jc_pin9_io : inout STD_LOGIC;
    led_16bits_tri_io : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    push_buttons_4bits_tri_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ready_tri_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset : in STD_LOGIC;
    sys_clock : in STD_LOGIC
  );
end component;
component UARTRec is
    Port ( clk          : in  STD_LOGIC;
       s_rx_sig_in  : in  STD_LOGIC;
       rdy_bit      : in  STD_LOGIC;
       message_out  : out STD_LOGIC_VECTOR (7 downto 0)
     --  bit_strm_out : out STD_LOGIC_VECTOR (7 downto 0);
     --  sw           : in  STD_LOGIC;

       --led          : out STD_LOGIC_VECTOR (0 to 7)  
       --rx_byte_rdy  : out STD_LOGIC
         );
end component;
begin
E1  :   oledrgbpmod_wrapper port map(count,
                                    JB(0),JB(1),JB(2),JB(3),JB(4),JB(5),JB(6),JB(7),
                                    JC(0),JC(1),JC(2),JC(3),JC(4),JC(5),JC(6),JC(7),
                                    leds(15 downto 0), 
                                    btn(4 downto 1),
                                    rdy, 
                                    btn(0), 
                                    clk);
U1  :   UARTRec port map(clk,
                        JA(2),
                        rdy(0),
                        count);
end Behavioral;
