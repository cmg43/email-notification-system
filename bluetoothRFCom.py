#!/usr/bin/env python

#######################################################
# File Name : bluetoothRFCom.py
                                                     
#Purpose : Bluetooth RFComm Client

#Creation Date : 14-04-2018

#Author :  A.R. Cowie
########################################################
import bluetooth
import os 


class bluetoothRFCom:
    
    """Class used to handle bluetooth communication"""
    
    @staticmethod
    def connectToBT(addr, port = None):

        """Function to establish the connection/socket
        Keyword arguments:
        addr -- holds the address for the bluetooth device, it's MAC ID
        pt   -- specifies the port for the socket, typically 1
        """
        if port is None:
            port = 1

        btPmodAddr = addr

        sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        sock.connect((btPmodAddr, port))

        if sock.getpeername()[0] == btPmodAddr:
            print "Connection to {} sucessful".format(btPmodAddr)
            return sock 
        else:
            print "Connection failed to {}".format(sock.getpeername()[0])
            x = raw_input("Try again? y/n ")
            if x == "y":
                connectToBT(btPmodAddr, port)
            else:
                return sock 

    @staticmethod
    def sendMsg(sock, msg):

        """Function responsible for sending the message over the specified socket
        Keyword arguments:
        sock -- The socket to send the message over
        msg  -- the message to be sent
        """

        print ("Sending message: " + msg )
        sock.send(msg)

    @staticmethod
    def closeCon(sock):
    
        """Simple function to close the socket when done.
        Keyword arguments:
        sock -- Holds the object that represents the socket
        """

        sock.close()


    if __name__ == '__main__':
        
        """The code in this function was designed to test the project."""
        
        btPmodAddr = "00:06:66:D0:C2:7B"
        port = 1
        print "Establishing Connection to {}, on port {}".format(btPmodAddr, port)
        sock = connectToBT(btPmodAddr, port)
        msg = raw_input("Please press a character to send or - to quit: ")
        sendMsg(sock, msg)
        while msg != '-':
            msg = raw_input("Please press a character to send or - to quit: ")
            sendMsg(sock, msg)
    
        print ("Closing connection to " + btPmodAddr)
        closeCon(sock)



    
