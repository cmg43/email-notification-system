#!/usr/bin/env python

#######################################################
# File Name : googleAuth.py
                                                     
#Purpose :  Script for obtaining OAuth2 for interfacing with Google API's

#Creation Date : 11-04-2018

#Author :  A.R. Cowie
########################################################

from __future__ import print_function
import httplib2
import os
import time

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from google.cloud import pubsub

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None


class googleAuth:
    """ If modifying these scopes, delete your previously saved credentials
    SCOPES = 'https://www.googleapis.com/auth/pubsub'
    CLIENT_SECRET_FILE = 'client_secret.json'
    APPLICATION_NAME = 'Gmail Notification Project'"""
    
    @staticmethod
    def get_credentials(SCOPES, CLIENT_SECRET_FILE, APPLICATION_NAME):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
        Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,'gmail-python-notification-project.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
                print('Storing credentials to ' + credential_path)
        return credentials


