----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Albert Cowie
-- 
-- Create Date: 02/28/2018 02:27:38 PM
-- Design Name: 
-- Module Name: DFlipFlop - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: D flip Flop based on code from cLASS
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DFlipFlop is
    Port ( clk  : in    STD_LOGIC :='0';
           T    : in    STD_LOGIC :='0';
           rst  : in    STD_LOGIC :='0';
           Q    : out   STD_LOGIC :='0';
           notQ : out   STD_LOGIC :='0'
           );
end DFlipFlop;

architecture Behavioral of DFlipFlop is

begin 
    process(clk,rst)
           begin
                if (rst='1') then
                    Q <= '0';
                elsif (rising_edge(clk)) then
                     Q <= T;
                     notQ <= not T;
                 end if;
                 
     end process;
end Behavioral;
