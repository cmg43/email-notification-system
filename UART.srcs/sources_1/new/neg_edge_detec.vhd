----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Albert R Cowie
-- 
-- Create Date: 04/30/2018 02:59:02 PM
-- Design Name: 
-- Module Name: neg_edge_detec - Behavioral
-- Project Name: Email Notification 
-- Target Devices: FPGA
-- Tool Versions: 
-- Description: Negative edge detector for signals
-- 
-- Dependencies: DFlipFlop
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity neg_edge_detec is
    Port ( clk        : in STD_LOGIC;
           signal_in  : in STD_LOGIC;
           signal_out : out STD_LOGIC);
end neg_edge_detec;

architecture Behavioral of neg_edge_detec is

component DFlipFlop PORT( clk  : in    STD_LOGIC :='0';
                          T    : in    STD_LOGIC :='0';
                          rst  : in    STD_LOGIC :='0';
                          Q    : out   STD_LOGIC :='0';
                          notQ : out   STD_LOGIC :='0'
                        );
end component;

SIGNAL  rst             :   STD_LOGIC := '0';
SIGNAL  DffConnector    :   STD_LOGIC := '0';
SIGNAL  Grnd            :   STD_LOGIC := '0';
SIGNAL  OutConnector    :   STD_LOGIC := '0';

begin


FF1 :   DFlipFlop   PORT MAP( clk   => clk,
                              T     => signal_in,
                              rst   => rst,
                              Q     => DffConnector,
                              notQ  => Grnd
                            );
                            
FF2 :   DFlipFlop   PORT MAP( clk   => clk,
                              T     => DffConnector,
                              rst   => rst,
                              Q     => OutConnector,
                              notQ  => Grnd
                            );
 
 neg_edge_det_algo : process(clk, rst)
 begin
    
    signal_out <= not DffConnector and OutConnector;
 
 end process;
    
 end Behavioral;
