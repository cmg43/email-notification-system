/******************************************************************************/
/*                                                                            */
/* MyDispDemo1.cc -- MyDisp Library Reference Example 1                       */
/*                                                                            */
/******************************************************************************/
/* Author: Gene Apperson                                                      */
/* Copyright 2016, Digilent Inc. All rights reserved.                         */
/******************************************************************************/
/* Module Description:                                                        */
/*                                                                            */
/* Among other things, this sketch demonstrates drawing images taken from     */
/* bitmap files stored on the SD card. The bitmaps drawn are contained in the */
/* resource zip file that accompanied the download of the demo projects.      */
/* Ensure that the bitmaps are in the Images folder on the SD card before     */
/* running the sketch.                                                        */
/*                                                                            */
/******************************************************************************/
/* Revision History:                                                          */
/*                                                                            */
/*    10/05/2016(GeneA):    Created                                           */
/*    02/14/2017(SamB):     Removed Serial references to port to Xilinx SDK   */
/*    12/09/2017(atangzwj): Validated for Vivado 2016.4                       */
/*    01/09/2018(atangzwj): Validated for Vivado 2017.4                       */
/*                                                                            */
/******************************************************************************/

/* ------------------------------------------------------------ */
/*                Include File Definitions                      */
/* ------------------------------------------------------------ */

#include <mtds.h>
#include <MyDisp.h>
#include <stdint.h>
#include <string.h>
#include "sleep.h"
#include "xil_cache.h"
#include "xparameters.h"
#include <string>
#include "xgpio.h"
//send data over UART
#include "xil_printf.h"

//information about AXI peripherals
#include "xparameters.h"


/* ------------------------------------------------------------ */
/*                Local Type Definitions                        */
/* ------------------------------------------------------------ */

/* ------------------------------------------------------------ */
/*                Global Variables                              */
/* ------------------------------------------------------------ */

/* ------------------------------------------------------------ */
/*                Local Variables                               */
/* ------------------------------------------------------------ */


int count=0;
XGpio gpio,gpio1;
u32 ready,command = 0x00000000;
MTDS mtd;
/* ------------------------------------------------------------ */
/*                Forward Declarations                          */
/* ------------------------------------------------------------ */
void MyDispTest1();
void MyDispTest2();
void MyDispTest3();
void MyDispTest4();
void iotesting();
void idle();
void newNotification();

/* ------------------------------------------------------------ */
/*                Procedure Definitions                         */
/* ------------------------------------------------------------ */

/*** setup()
**
**   Parameters:
**      none
**
**   Return Values:
**      none
**
**   Errors:
**      none
**
**   Description:
**      Arduino/MPIDE sketch initialization function.
*/


void setup() {

   bool fStat;


   /* The following initializes the library and the communications interface to
   ** the display board. It is quite possible that the display board hasn't
   ** completed its power on or reset initialization at this point and the
   ** begin() function will fail. Because of this it's a good idea to run the
   ** begin() function in a loop in case it fails the first time. Keep looping
   ** until it succeeds.
   */
   while (true) {
      fStat = mydisp.begin();
      if (fStat) {
         xil_printf("mydisp.begin() succeeded\n\r");
         break;
      }
      else {
         xil_printf("mydisp.begin() failed\n\r");
         sleep(1);
      }
   }
}

void iotesting(){


	   	u32 led;

	   	XGpio_Initialize(&gpio, 0);
	   	XGpio_SetDataDirection(&gpio, 2, 0x00000000); // set LED GPIO channel tristates to All Output

	   	XGpio_Initialize(&gpio1,1);
	   	XGpio_SetDataDirection(&gpio1, 1, 0XFFFFFFFF); // set uart bluetooth GPIO channel to ALL Input
	   	XGpio_SetDataDirection(&gpio1, 2, 0X00000000); // set ready bit GPIO channel to All Output
	   	if (ready == 1)
	   		{command  = XGpio_DiscreteRead(&gpio1, 1);}
	   	led = command;
	   	XGpio_DiscreteWrite(&gpio, 2, led);
}
/* ------------------------------------------------------------ */
/*** loop()
**
**   Parameters:
**      none
**
**   Return Values:
**      none
**
**   Errors:
**      none
**
**   Description:
**      Arduino/MPIDE main sketch function
*/
void loop() {
   mydisp.clearDisplay(clrBlack);
   mtd.SetDisplayOrientation(2);//set display orientation to landscape 1 for portrait default portrait
   iotesting();
   /* Uncomment the following line and assign the test number to itstCur to
   ** cause a specific test to be repeatedly displayed.
   */
   //itstCur = 1;
   switch (command) {
   case 0://when not getting new notification put in idle state command received
      idle();
      break;

   case 1://new notification
      newNotification();
      break;

   }

   //sleep(1);
}






void idle(){
	int sleeptime = 1;
	ready = 31250;
	XGpio_DiscreteWrite(&gpio1, 2, ready);//write ready bit to gpio port
	char buffer[10];
	mydisp.setPen(penSolid);
	mydisp.setForeground(clrGreen);
	mydisp.setBackground(clrBlack);
	itoa(count,buffer,10);
	mydisp.drawImage((char*) "Images/idlemario1.bmp", 0, 0);//draw first frame of idle animation
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	//usleep(sleeptime);//sleep so that 32 frames are drawn per second
	/*mydisp.drawImage((char*) "Images/idlemario2.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
    //usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario3.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	//usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario4.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	//usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario5.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	//usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario6.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	//usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario7.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	//usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario8.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario9.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario10.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario11.bmp", 0, 0);//draw first frame of idle animation
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario12.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
    usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario13.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario14.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario15.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario16.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario17.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario18.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario19.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario20.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario21.bmp", 0, 0);//draw first frame of idle animation
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario22.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
    usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario23.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario24.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario25.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario26.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario27.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario28.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario29.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario30.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario31.bmp", 0, 0);//draw first frame of idle animation
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario32.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
    usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario33.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario34.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario35.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario36.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario37.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario38.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario39.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario40.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario41.bmp", 0, 0);//draw first frame of idle animation
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario42.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
    usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario43.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario44.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario45.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario46.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario47.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario48.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario49.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario50.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario51.bmp", 0, 0);//draw first frame of idle animation
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario52.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
    usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario53.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario54.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario55.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario56.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario57.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario58.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario59.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario60.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario61.bmp", 0, 0);//draw first frame of idle animation
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario62.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
    usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario63.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second
	mydisp.drawImage((char*) "Images/idlemario64.bmp", 0, 0);
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
	usleep(sleeptime);//sleep so that 32 frames are drawn per second*/
}


void newNotification(){
	ready=0;//set ready bit to 0
	XGpio_DiscreteWrite(&gpio1, 2, ready);//write bit to gpio port
	count++;//increment total of unacknowledged notifications
	//draw new notification bmps
	command=0;//set command back to idle
}
