/******************************************************************************/
/*                                                                            */
/* emailns.cpp  email notification to handle logic between bluetooth and mtds */
/*                                                                            */
/******************************************************************************/
/* Author: Chris Golinski, Albert Cowie                                       */
/*                        													  */
/******************************************************************************/
/* Module Description:                                                        */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/* Revision History:                                                          */
/*                                                                            */
/******************************************************************************/

/* ------------------------------------------------------------ */
/*                Include File Definitions                      */
/* ------------------------------------------------------------ */

#include <mtds.h>
#include <MyDisp.h>
#include <stdint.h>
#include <string.h>
#include "sleep.h"
#include "xil_cache.h"
#include "xparameters.h"
#include <string>
#include "xgpio.h"
//send data over UART
#include "xil_printf.h"

//information about AXI peripherals
#include "xparameters.h"


/* ------------------------------------------------------------ */
/*                Local Type Definitions                        */
/* ------------------------------------------------------------ */

/* ------------------------------------------------------------ */
/*                Global Variables                              */
/* ------------------------------------------------------------ */

/* ------------------------------------------------------------ */
/*                Local Variables                               */
/* ------------------------------------------------------------ */

int step=0;
const int maxstep = 63;
int count=0;
int newnotes=0;
XGpio gpio,gpio1;
u32 led,ready,command = 0x00000000;
MTDS mtd;
/* ------------------------------------------------------------ */
/*                Forward Declarations                          */
/* ------------------------------------------------------------ */

void idle();
void newNotification();

/* ------------------------------------------------------------ */
/*                Procedure Definitions                         */
/* ------------------------------------------------------------ */

/*** setup()
**
**   Parameters:
**      none
**
**   Return Values:
**      none
**
**   Errors:
**      none
**
**   Description:
**      Arduino/MPIDE sketch initialization function.
*/


void setup() {

   bool fStat;
  	XGpio_Initialize(&gpio, 0);
  	XGpio_SetDataDirection(&gpio, 2, 0x00000000); // set LED GPIO channel tristates to All Output

  	XGpio_Initialize(&gpio1,1);
  	XGpio_SetDataDirection(&gpio1, 1, 0XFFFFFFFF); // set uart bluetooth GPIO channel to ALL Input
  	XGpio_SetDataDirection(&gpio1, 2, 0X00000000); // set ready bit GPIO channel to All Output

   /* The following initializes the library and the communications interface to
   ** the display board. It is quite possible that the display board hasn't
   ** completed its power on or reset initialization at this point and the
   ** begin() function will fail. Because of this it's a good idea to run the
   ** begin() function in a loop in case it fails the first time. Keep looping
   ** until it succeeds.
   */
   while (true) {
      fStat = mydisp.begin();
      if (fStat) {
         xil_printf("mydisp.begin() succeeded\n\r");
         break;
      }
      else {
         xil_printf("mydisp.begin() failed\n\r");
         sleep(1);
      }
   }
}


/* ------------------------------------------------------------ */
/*** loop()
**
**   Parameters:
**      none
**
**   Return Values:
**      none
**
**   Errors:
**      none
**
**   Description:
**      Arduino/MPIDE main sketch function
*/
void loop() {
   //mydisp.clearDisplay(clrBlack);
   //mtd.SetDisplayOrientation(1);//set display orientation to landscape 1 for portrait default portrait
	XGpio_DiscreteWrite(&gpio1, 2, ready);//write bit to gpio port
   	newnotes  = XGpio_DiscreteRead(&gpio1, 1);
   	led = count;
   	XGpio_DiscreteWrite(&gpio, 2, led);
   	if (newnotes > 0){
   		ready=0;
   		command=1;}
   	else{
   		ready=1;
   		command=0;}

   /* Uncomment the following line and assign the test number to itstCur to
   ** cause a specific test to be repeatedly displayed.
   */
   //itstCur = 1;
   switch (command) {
   case 0://when not getting new notification put in idle state command received
      idle();
      break;

   case 1://new notification
      newNotification();
      break;

   }
   if(step < maxstep)//if havent drawn last frame of mario animation
	   step++;//increment step
   else
	   step=0;//set step to 0
}






void idle(){
	char buffer[10];//setup buffer used to printout the count of unacknowledged notifications
	mydisp.setPen(penSolid);//set text type
	mydisp.setForeground(clrWhite);//set text color
	mydisp.setBackground(clrBlack);//set text background
	itoa(count,buffer,10);//turn count into a string
	switch(step){//draw image for current step in animation
	case(0):
		mydisp.drawImage((char*) "Images/idlemario1.bmp", 0, 0);
		break;
	case(1):
		mydisp.drawImage((char*) "Images/idlemario2.bmp", 0, 0);
		break;
	case(2):
		mydisp.drawImage((char*) "Images/idlemario3.bmp", 0, 0);
		break;
	case(3):
		mydisp.drawImage((char*) "Images/idlemario4.bmp", 0, 0);
		break;
	case(4):
		mydisp.drawImage((char*) "Images/idlemario5.bmp", 0, 0);
		break;
	case(5):
		mydisp.drawImage((char*) "Images/idlemario6.bmp", 0, 0);
		break;
	case(6):
		mydisp.drawImage((char*) "Images/idlemario7.bmp", 0, 0);
		break;
	case(7):
		mydisp.drawImage((char*) "Images/idlemario8.bmp", 0, 0);
		break;
	case(8):
		mydisp.drawImage((char*) "Images/idlemario9.bmp", 0, 0);
		break;
	case(9):
		mydisp.drawImage((char*) "Images/idlemario10.bmp", 0, 0);
		break;
	case(10):
		mydisp.drawImage((char*) "Images/idlemario11.bmp", 0, 0);
		break;
	case(11):
		mydisp.drawImage((char*) "Images/idlemario12.bmp", 0, 0);
		break;
	case(12):
		mydisp.drawImage((char*) "Images/idlemario13.bmp", 0, 0);
		break;
	case(13):
		mydisp.drawImage((char*) "Images/idlemario14.bmp", 0, 0);
		break;
	case(14):
		mydisp.drawImage((char*) "Images/idlemario15.bmp", 0, 0);
		break;
	case(15):
		mydisp.drawImage((char*) "Images/idlemario16.bmp", 0, 0);
		break;
	case(16):
		mydisp.drawImage((char*) "Images/idlemario17.bmp", 0, 0);
		break;
	case(17):
		mydisp.drawImage((char*) "Images/idlemario18.bmp", 0, 0);
		break;
	case(18):
		mydisp.drawImage((char*) "Images/idlemario19.bmp", 0, 0);
		break;
	case(19):
		mydisp.drawImage((char*) "Images/idlemario20.bmp", 0, 0);
		break;
	case(20):
		mydisp.drawImage((char*) "Images/idlemario21.bmp", 0, 0);
		break;
	case(21):
		mydisp.drawImage((char*) "Images/idlemario22.bmp", 0, 0);
		break;
	case(22):
		mydisp.drawImage((char*) "Images/idlemario23.bmp", 0, 0);
		break;
	case(23):
		mydisp.drawImage((char*) "Images/idlemario24.bmp", 0, 0);
		break;
	case(24):
		mydisp.drawImage((char*) "Images/idlemario25.bmp", 0, 0);
		break;
	case(25):
		mydisp.drawImage((char*) "Images/idlemario26.bmp", 0, 0);
		break;
	case(26):
		mydisp.drawImage((char*) "Images/idlemario27.bmp", 0, 0);
		break;
	case(27):
		mydisp.drawImage((char*) "Images/idlemario28.bmp", 0, 0);
		break;
	case(28):
		mydisp.drawImage((char*) "Images/idlemario29.bmp", 0, 0);
		break;
	case(29):
		mydisp.drawImage((char*) "Images/idlemario30.bmp", 0, 0);
		break;
	case(30):
		mydisp.drawImage((char*) "Images/idlemario31.bmp", 0, 0);
		break;
	case(31):
		mydisp.drawImage((char*) "Images/idlemario32.bmp", 0, 0);
		break;
	case(32):
		mydisp.drawImage((char*) "Images/idlemario33.bmp", 0, 0);
		break;
	case(33):
		mydisp.drawImage((char*) "Images/idlemario34.bmp", 0, 0);
		break;
	case(34):
		mydisp.drawImage((char*) "Images/idlemario35.bmp", 0, 0);
		break;
	case(35):
		mydisp.drawImage((char*) "Images/idlemario36.bmp", 0, 0);
		break;
	case(36):
		mydisp.drawImage((char*) "Images/idlemario37.bmp", 0, 0);
		break;
	case(37):
		mydisp.drawImage((char*) "Images/idlemario38.bmp", 0, 0);
		break;
	case(38):
		mydisp.drawImage((char*) "Images/idlemario39.bmp", 0, 0);
		break;
	case(39):
		mydisp.drawImage((char*) "Images/idlemario40.bmp", 0, 0);
		break;
	case(40):
		mydisp.drawImage((char*) "Images/idlemario41.bmp", 0, 0);
		break;
	case(41):
		mydisp.drawImage((char*) "Images/idlemario42.bmp", 0, 0);
		break;
	case(42):
		mydisp.drawImage((char*) "Images/idlemario43.bmp", 0, 0);
		break;
	case(43):
		mydisp.drawImage((char*) "Images/idlemario44.bmp", 0, 0);
		break;
	case(44):
		mydisp.drawImage((char*) "Images/idlemario45.bmp", 0, 0);
		break;
	case(45):
		mydisp.drawImage((char*) "Images/idlemario46.bmp", 0, 0);
		break;
	case(46):
		mydisp.drawImage((char*) "Images/idlemario47.bmp", 0, 0);
		break;
	case(47):
		mydisp.drawImage((char*) "Images/idlemario48.bmp", 0, 0);
		break;
	case(48):
		mydisp.drawImage((char*) "Images/idlemario49.bmp", 0, 0);
		break;
	case(49):
		mydisp.drawImage((char*) "Images/idlemario50.bmp", 0, 0);
		break;
	case(50):
		mydisp.drawImage((char*) "Images/idlemario51.bmp", 0, 0);
		break;
	case(51):
		mydisp.drawImage((char*) "Images/idlemario52.bmp", 0, 0);
		break;
	case(52):
		mydisp.drawImage((char*) "Images/idlemario53.bmp", 0, 0);
		break;
	case(53):
		mydisp.drawImage((char*) "Images/idlemario54.bmp", 0, 0);
		break;
	case(54):
		mydisp.drawImage((char*) "Images/idlemario55.bmp", 0, 0);
		break;
	case(55):
		mydisp.drawImage((char*) "Images/idlemario56.bmp", 0, 0);
		break;
	case(56):
		mydisp.drawImage((char*) "Images/idlemario57.bmp", 0, 0);
		break;
	case(57):
		mydisp.drawImage((char*) "Images/idlemario58.bmp", 0, 0);
		break;
	case(58):
		mydisp.drawImage((char*) "Images/idlemario59.bmp", 0, 0);
		break;
	case(59):
		mydisp.drawImage((char*) "Images/idlemario60.bmp", 0, 0);
		break;
	case(60):
		mydisp.drawImage((char*) "Images/idlemario61.bmp", 0, 0);
		break;
	case(61):
		mydisp.drawImage((char*) "Images/idlemario62.bmp", 0, 0);
		break;
	case(62):
		mydisp.drawImage((char*) "Images/idlemario63.bmp", 0, 0);
		break;
	case(63):
		mydisp.drawImage((char*) "Images/idlemario64.bmp", 0, 0);
	}
	mydisp.drawText((char*) buffer, 0, 0);//draw total unacknowledged notifications on top of idle animation
}


void newNotification(){
	count=count+newnotes;//increment total of unacknowledged notifications
	//newnotes=0;
	//draw new notification bmps
	mydisp.drawImage((char*) "Images/IMG_0100.BMP", 0, 0);//draw first frame of idle animation
}
