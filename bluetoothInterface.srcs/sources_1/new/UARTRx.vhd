----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Albert R Cowie
-- 
-- Create Date: 04/13/2018 03:57:29 PM
-- Design Name: 
-- Module Name: UARTRx - Behavioral
-- Project Name: Email Notification 
-- Target Devices: FPGA
-- Tool Versions: 
-- Description: Module hands receiving UART communications
-- 
-- Dependencies: None
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
--shiftregister(7 downto 0) = "00000000"
--shiftresgietr <= shiftregister(6 downto 0) & '1';

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UARTRx is
    Port ( clk          : in  STD_LOGIC;
           s_rx_sig_in  : in  STD_LOGIC;
         --  bit_strm_out : out STD_LOGIC_VECTOR (7 downto 0);
           btnC         : in  STD_LOGIC;
           led          : out STD_LOGIC_VECTOR (0 to 7) 
           --state_out    : out STD_LOGIC_VECTOR (2 downto 0)  
           --rx_byte_rdy  : out STD_LOGIC
         );
end UARTRx;

architecture Behavioral of UARTRx is


type    rx_state is (idle, start_bit, data_bit, stop_bit, cleanup); --enumed types for controling state of the UART Receiver

signal  s_rx_state       : rx_state := idle;
signal  clk_rate         : integer  := 100000000;
signal  baud_rate        : integer  := 9600;
signal  clk_tics_per_bit : integer  := 1041;                    --clk_rate/baud_rate
signal  clk_count        : integer := 0;                    -- range 0 to clk_tics_per_bit;
signal  bit_index        : integer range 0 to 7;
signal  i_clk            : STD_LOGIC;
signal  bit_strm_accum   : STD_LOGIC_VECTOR (0 to 7) := (others => '0');
signal  rx_sig_in        : STD_LOGIC := '1';
signal  rst              : STD_LOGIC := '0';
--signal  led              : STD_LOGIC_VECTOR (4 downto 0) := (others => '0');
signal st_out            : STD_LOGIC_VECTOR (2 downto 0) := (others => '0');   
signal enable            : STD_LOGIC
signal temp              : STD_LOGIC
begin

--led_out <= led;
i_clk <= clk;
rst <= btnC;

process(clk)
    begin
        if rst = '1' then
                s_rx_state      <= idle;
                clk_count       <= 0;
                bit_index       <= 0;
                rx_sig_in       <= '1';
                bit_strm_accum  <= (others => '0');
                --led             <= (others => '0');
                --clk_tics_per_bit <= clk_rate/baud_rate;
             --   bit_strm_out    <= (others => '0');
            end if;
        if rising_edge(clk) then
            case s_rx_state is
            
                when idle =>
                  --  led <= "00001";
                 -- state_out <= "000";
                    bit_index <= 0;
                    clk_count <= 0;
               
                    if s_rx_sig_in = '0' then      --start bit detected
                        s_rx_state <= start_bit;
                    else
                        s_rx_state <= idle;
                    end if;
                    
                when start_bit =>
                   -- state_out <= "001";
                    --led <= "00010";
                    if clk_count >= clk_tics_per_bit/2 then  --found the middle of the start bit signal
                        if s_rx_sig_in = '0' then             --double check start bit still 0
                            s_rx_state <= data_bit;
                            clk_count  <= 0;                --reset clk_count to 0 to start sampleing data bits              
                        else
                            s_rx_state <= idle;             --something went wrong with start bit
                        end if;
                    else
                        s_rx_state <= start_bit;
                        clk_count <= clk_count + 1;
                    end if;
                    
                when data_bit =>
                    --state_out <= "010";
                   -- led <= "00100";
                    if clk_count < clk_tics_per_bit then    --need to wait 1 full cycle to keep in the middle of the stream
                        clk_count   <= clk_count + 1;
                        s_rx_state  <= data_bit;
                    else
                        if bit_index < 8 then                           --indicates still more data bits
                            bit_strm_accum(bit_index)   <= s_rx_sig_in;
                            --bit_strm_out                <= bit_strm_accum;
                            bit_index                   <= bit_index + 1;
                            clk_count                   <= 0;
                            s_rx_state                  <= data_bit;
                        else    
                     --       bit_strm_out                <= bit_strm_accum;   --at the end of the data bits
                            s_rx_state                  <= stop_bit;
                            clk_count                   <= 0;
                        end if;
                    end if;
                    
                 when stop_bit =>
                    clk_count <= 0;
                   -- state_out <= "011";
                    --led <= "01000";
                    if clk_count < clk_tics_per_bit then
                        clk_count   <= clk_count + 1;
                        s_rx_state  <= stop_bit;
                    else
                        s_rx_state  <= cleanup;
                    end if;
                    
                 when cleanup =>                    --reset and wait for next transimission
                    --state_out <= "100";
                    --led <= "10000";  
                     
                     --if bit_strm_accum = "1000010" then
                         --led <= "00001";
                     --else
                       --  led <= "00010";
                     --end if;                 
                    clk_count   <= 0;
                    bit_index   <= 0;
                    s_rx_state  <= idle;
                    
                 when others =>
                 --   state_out <= "111";
                    s_rx_state <= idle;
                    
                 end case;
           end if;
end process;

--display : process(clk, rst)
  --  begin
    --    if enable = '1' then
      --      led <= bit_strm_accum;
        --else
--end process;

                    
end Behavioral;
