----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Albert R Cowie
-- 
-- Create Date: 04/13/2018 03:57:29 PM
-- Design Name: 
-- Module Name: UARTRx - Behavioral
-- Project Name: Email Notification 
-- Target Devices: FPGA
-- Tool Versions: 
-- Description: Module hands receiving UART communications
-- 
-- Dependencies: None
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
--shiftregister(7 downto 0) = "00000000"
--shiftresgietr <= shiftregister(6 downto 0) & '1';

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UARTRec is
    Port ( clk          : in  STD_LOGIC;
           s_rx_sig_in  : in  STD_LOGIC;
           rdy_bit      : in  STD_LOGIC;    --Is 1 when the LCD screen is ready for input and 0 when it is not
           message_out  : out STD_LOGIC_VECTOR (7 downto 0)
         );
end UARTRec;

architecture Behavioral of UARTRec is

Component neg_edge_detec Port ( clk : in STD_LOGIC;
                                signal_in  : in STD_LOGIC;
                                signal_out : out STD_LOGIC
                              );
end Component;


type    rx_state is (idle, start_bit, data_bit, stop_bit, cleanup); --enumed types for controling state of the UART Receiver

signal  s_rx_state       : rx_state := idle;
signal  clk_rate         : integer  := 100000000;
signal  baud_rate        : integer  := 9600;
signal  clk_tics_per_bit : integer  := 10416;                    --clk_rate/baud_rate
signal  clk_count        : integer := 0;                    -- range 0 to clk_tics_per_bit;
signal  bit_index        : integer range 0 to 8;
signal  i_clk            : STD_LOGIC;
signal  bit_strm_accum   : STD_LOGIC_VECTOR (0 to 7) := (others => '0');
signal  rx_sig_in        : STD_LOGIC := '1'; 
signal  ready_to_send    : STD_LOGIC := '0';
signal  neg_edge_check   : STD_LOGIC := '0';


begin

neg_edge_comp : neg_edge_detec PORT MAP( clk => clk,
                                         signal_in => rdy_bit,
                                         signal_out => neg_edge_check
                                       );

process(clk)
    begin
        if rising_edge(clk) then
            case s_rx_state is
            
                when idle =>
                    bit_index <= 0;
                    clk_count <= 0;
                    
                    --message_out<="00000011";
                    if s_rx_sig_in = '0' then      --start bit detected
                        s_rx_state <= start_bit;
                    else
                        s_rx_state <= idle;
                    end if;
                    
                when start_bit =>
                --message_out<="00000100";
                    if clk_count >= clk_tics_per_bit/2 then  --found the middle of the start bit signal
                        if s_rx_sig_in = '0' then             --double check start bit still 0
                            s_rx_state <= data_bit;
                            clk_count  <= 0;                --reset clk_count to 0 to start sampleing data bits              
                        else
                            s_rx_state <= idle;             --something went wrong with start bit
                        end if;
                    else
                        s_rx_state <= start_bit;
                        clk_count <= clk_count + 1;
                    end if;
                    
                when data_bit =>
                --message_out<="00000101";
                    if clk_count < clk_tics_per_bit then    --need to wait 1 full cycle to keep in the middle of the stream
                        clk_count   <= clk_count + 1;
                        s_rx_state  <= data_bit;
                    else
                        if bit_index < 8 then                           --indicates still more data bits
                                                       
                            bit_strm_accum(bit_index)   <= s_rx_sig_in;
                            --bit_strm_out                <= bit_strm_accum;
                            bit_index                   <= bit_index + 1;
                            clk_count                   <= 0;
                            s_rx_state                  <= data_bit;
                        else   
                            
                     --       bit_strm_out                <= bit_strm_accum;   --at the end of the data bits
                            clk_count                   <= 0;
                            s_rx_state                  <= stop_bit;
                        end if;
                    end if;
                    
                 when stop_bit =>
                 --message_out<="00000110";
                    if clk_count < clk_tics_per_bit then
                        clk_count   <= clk_count + 1;
                        s_rx_state  <= stop_bit;
                    else
                        ready_to_send <= '1';
                        s_rx_state  <= cleanup;
                    end if;
                    
                 when cleanup =>                    --reset and wait for next byte               
                    clk_count   <= 0;
                    bit_index   <= 0;
                    s_rx_state  <= idle;
                    
                 when others =>
                    s_rx_state <= idle;
                    
                 end case;
                 
                 -- This section of code handles when to send a message to the LCD screen and when to stop sending
                 if neg_edge_check = '1' then
                    message_out <= "00000000";
                    ready_to_send <= '0';  
                 end if; 
                 
                 if rdy_bit = '1' then
                    if ready_to_send = '1' then
                       message_out <= "00000001";
                    else
                        message_out <= "00000000";
                    end if;
                 end if;
                 
              end if;             
           
end process;

--display : process(clk)
--    begin
--        if rdy_bit = '1' then
--            if s_rx_state = stop_bit then
                --temp <= bit_strm_accum;
               -- temp <= "00000001";  
 --               message_out <= "00000001";
                
 --               enable <= '0';   
 --           end if;  
 --       else
 --           message_out <= "00000000";   
 --       end if;
 --       
 --     if s_rx_state = data_bit then
 --          enable <= '1';
 --       
          -- led <= (others => '0');
 --     end if;
        --led <= temp;
    --    message_out <= temp;
--end process;

                    
end Behavioral;

