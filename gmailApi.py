#!/usr/bin/env python

#######################################################
# File Name : gmailApi.py
                                                     
#Purpose : Interface with google API to get watch notifications

#Creation Date : 07-04-2018

#Author :  A.R. Cowie
########################################################

from __future__ import print_function
import httplib2
import os
import time

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from google.cloud import pubsub
from googleAuth import googleAuth
from bluetoothRFCom import bluetoothRFCom


try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

def watchSetup(gmail):
    
    """This function sets up the the the watch for the push style notifications

    Key Arguments:
    gmail -- This contains the needed information for the API, specifing, which API,
             gmail, the version - v1, and the type of request - http
    """

    request = {
                'labelIds': ['INBOX'],
                'topicName': 'projects/email-notification-project/topics/newEmail'
                }
    gmail.users().watch(userId='me', body=request).execute()
            

def receive_messages(project, subscription_name):
    
    """This function handles receiving the noitifactions from the google cloud
    pub/sub API

    Key arguments:
    project -- this is the string that contains the name of the project.  Has to match
               the project name on google cloud
    subscription_name -- string that contains the name of the subscription from the google
                         cloud pub/sub API
    """
    
    sock = bluetoothRFCom.connectToBT('00:06:66:D0:C2:7B')

    subscriber = pubsub.SubscriberClient()
    subscription_path = subscriber.subscription_path(project, subscription_name)

    def callback(message):
        print('Received message: {}'.format(message))
        bluetoothRFCom.sendMsg(sock, 'A')
        message.ack()

    subscriber.subscribe(subscription_path, callback=callback)

    # The subscriber is non-blocking, so we must keep the main thread from
    # exiting to allow it to process messages in the background.
    print('Listening for messages on {}'.format(subscription_path))
    while True:
        time.sleep(60)
                

def main():

    """Main function initializes setup"""
    
    #Obtains OAuth2 credentials for setting the watch on gmail
    
    credentials = googleAuth.get_credentials('https://www.googleapis.com/auth/gmail.readonly', 
                                            'client_secret.json', 'Gmail Notification Project')
    
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)
    watchSetup(service)
    
    #Sets the enviromental variable needed which google cloud looks for to be able to receive messages
    os.system('export GOOGLE_APPLICATION_CREDENTIALS="~/class/CSE590/finalProject/EMAIL_NOTIFICATION_PROJECT.json"')

    receive_messages('email-notification-project', 'newEmailSub')
      
      


if __name__ == '__main__':
    
    """Standard entry point into a python program"""
    
    main()
